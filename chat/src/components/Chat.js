import React from 'react'
import socketIOClient from "socket.io-client"
const ENDPOINT = "http://127.0.0.1:4000";

class Chat extends React.Component {

    constructor(props) {
        // setting ass the states that is needed
        super(props);
        this.state = {
            messages: [],
            users: [],
            username: null,
            message: null,
            sendMessage: null,
            color: null,
            socket: socketIOClient(ENDPOINT, 
                {transportOptions: {
                polling: {
                    extraHeaders: {
                        Authorization: this.props.location.state.token
                        
                    }
                }
            }}),
            chat: null,

        }
    }
    // loading the sockets 
    componentDidMount = async () => { 
        let socket = this.state.socket
        
        // setting the stats  with the values from the login page 
        this.setState({
            username: this.props.location.state.username,
            chat: this.props.location.state.chat
        })
        
        // creating data to be send to the server 
        await console.log("Sending data to server")
        let userAndChat = {
            username: this.state.username,
            chat: this.state.chat
        }
        
        // sending to sercer 
        socket.emit('joinRoom', userAndChat)
     
        // getting message from server 
        socket.on("message", data => {
            this.setState({
                messages: [...this.state.messages, data]

            })
        });

        // getting the users to each room from server 
        socket.on('Users', ({ room, usersFromRoom}) => {
            this.state.users = []
            usersFromRoom.forEach(element => {
                this.setState({
                    users: [...this.state.users, element.username]
    
                })
            });
    
              
            })
            // if the token is expired of not vaild the server will send back a message and the 
            // font-end will will routet to the login page 
        socket.on("expired", data => {
            this.props.history.push("/")

        })
    }

    // gettting the mesasge that is printed in the inpuy field 
    handleChange = async (event) => {
        console.log(event.target.value)
        await this.setState({
            sendMessage: event.target.value
        })
        
    }
    // sending the message to the server 
    sendMessage() {
        
        console.log("sending to server")
 

        this.state.socket.emit('chatMessage', this.state.sendMessage)

    }

    render() {
        return (


            <body>
                <div className="chat-container">
                    <header className="chat-header">
                        <h1><i className="fas fa-smile"></i>Chat</h1>
                    </header>
                    <main className="chat-main">
                        <div className="chat-sidebar">
                            <h3><i className="fas fa-comments"></i> Room Name:</h3>
                            <h2 id="room-name">{this.state.chat}</h2>
                            <h3><i className="fas fa-users"></i> Users in the chat</h3>
                            <ul id="users">
                                {
                                    this.state.users.map(user => {
                                        return (
                                        <li>{user}</li>
                                        )
                                    })
                                }

                            </ul>
                        </div>
                        <div id="chat" className="chat-messages">
                            
                                {
                                    this.state.messages.map(msg => {
                                        return (
                                            <div className = "message">
                                                <p className="meta">{msg.username}<span>{msg.time}</span></p>
                                                <p className="text" style={{color: this.state.color}} >{msg.text}</p>`
                                            </div>
                                        )
                                    })
                                }

                        </div>
                    </main>
                    <div className="chat-form-container">
                        <div>
                            <input
                                id="msg"
                                type="text"
                                placeholder="Enter Message"
                                required
                                autoComplete="off"
                                onChange={this.handleChange} />
                            <button className="btn" onClick={() => this.sendMessage()}>Send</button>
                        </div>
                    </div>
                </div>
            </body>
        )
    }
}

export default Chat