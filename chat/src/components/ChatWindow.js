import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCoffee } from '@fortawesome/free-solid-svg-icons'

class ChatWindow extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            username: null,
            chat: null,
            redirect: false


        }
    }
    // Setting the username 
    handleChangeUsername = async (event) => {
        await this.setState({
            username: event.target.value
        })
    }
    // setting the chat 
    handleChangeChat = async (event) => {
        await this.setState({
            chat: event.target.value
        })
    }
    /*
     * This method will take the values from the username and chat and send it to the server 
     * When the server will then sign a token to that username and send it back 
     * The token togheter with the username and chat will be send to the chat component 
     * 
    */
    sendToServer = async () => {

        const tokenFromAPI = await fetch("http://localhost:4000/getToken", {
            headers: {
                username: this.state.username
            }
        })
            .then((resp) => resp.json())
            .then((resp) => {
                console.log(resp)
                return resp
            })


        let data = {
            username: this.state.username,
            chat: this.state.chat,
            token: tokenFromAPI.token
        }
        // routing the to chat with the data 
        this.props.history.push("/chat", data)
    }
    render() {
        return (

            <body style={{ fontFamily: "sans-serif", fontSize: "16px", background: "var(--light-color)", margin: "20px" }}>
                <div className="join-container" style={{ maxWidth: "500px", margin: "80px auto" }}>
                    <header className="join-header" style={{ textAlign: "center", padding: "20px", background: "var(--dark-color-a)", borderTopLeftRadius: "5px", borderTopRightRadius: "5px" }}>
                        <h1><FontAwesomeIcon icon={faCoffee} /> ChatCord</h1>
                    </header>
                    <main className="join-main" style={{ padding: "30px 40px" }}>

                        <div className="form-control">
                            <label htmlFor="username">Username</label>
                            <input
                                type="text"
                                name="username"
                                id="username"
                                placeholder="Enter username..."
                                required
                                onChange={this.handleChangeUsername} />
                        </div>
                        <div className="form-control">
                            <label htmlFor="room">Room</label>
                            <select name="room" id="room" onChange={this.handleChangeChat}>
                                <option value="JavaScript">JavaScript</option>
                                <option value="Python">Python</option>
                                <option value="PHP">PHP</option>
                                <option value="C#">C#</option>
                                <option value="Ruby">Ruby</option>
                                <option value="Java">Java</option>
                            </select >
                        </div>
                        <button type="submit" className="btn" onClick={this.sendToServer}>Join Chat</button>

                    </main>
                </div>
            </body>
        )
    }
}

export default ChatWindow