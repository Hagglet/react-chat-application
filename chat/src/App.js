
import './App.css';
import React from "react";
import ChatWindow from '../src/components/ChatWindow'
import Chat from '../src/components/Chat'
import {
  BrowserRouter as Router,
  Route
} from "react-router-dom";

class App extends React.Component {


  render() {
    return (
      <Router>
        <div className="App">
        
          <Route exact path = "/" component ={ChatWindow}></Route>
          
          <Route path = "/chat" component ={Chat}/>
      
        </div>
      </Router>
    );
    }

}

export default App;
